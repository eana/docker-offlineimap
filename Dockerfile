FROM alpine:3.10
MAINTAINER Arthur Axel fREW Schmidt <frioux@gmail.com>

# Dockerfile defining a robust offlineimap container
#
# Copyright 2015 Arthur Axel fREW Schmidt
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.

ADD ./notmuch-config /home/user/.notmuch-config
ADD ./offlineimaprc /home/user/.offlineimaprc
ADD ./offlineimap.py /home/user/.offlineimap.py
ADD ./bin/generate_configs /usr/local/bin/generate_configs
ADD ./bin/cerberus /usr/local/bin/cerberus
ADD ./services /home/user/services

RUN set -ex && \
    apk add --update --no-cache python py-six git ca-certificates s6 notmuch py-pip && \
    cd /tmp && \
    git clone https://github.com/OfflineIMAP/offlineimap.git offlineimap && \
    cd offlineimap && \
	pip install rfc6555 && \
    python setup.py build && \
    python setup.py install && \
    cd / && \
    rm /tmp/offlineimap -rf && \
    adduser -D -h /home/user -u 1000 user && \
    mkdir -p /opt/var/mail /opt/var/index /opt/log && \
    ln -s /opt/etc/netrc /home/user/.netrc && \
    ln -s /opt/var/index /home/user/.offlineimap  && \
    chown 1000 /opt/var/mail /opt/var/index /home/user /opt/log -R  && \
    apk del git  && \
    rm -rf /usr/lib/python2.7/distutils  \
           /usr/lib/python2.7/idlelib    \
           /usr/lib/python2.7/lib-tk     \
           /usr/lib/python2.7/ensurepip  \
           /usr/lib/python2.7/pydoc_data \
           /var/cache/apk/*

# declaring the volume makes it owned by root.  Lame.
# VOLUME ["/opt/var/mail", "/opt/var/index", "/opt/log/offlineimap", "/opt/etc"]

USER user

CMD ["/usr/local/bin/generate_configs", "s6-svscan", "-S", "-t", "0", "/home/user/services"]
