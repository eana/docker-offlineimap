# offlineimap

Simple image to run [offlineimap](http://www.offlineimap.org/) from within a
docker image.

Without [Arthur Axel fREW Schmidt](https://github.com/frioux/offlineimap.dkr) work, this
project wouldn't be possible.

## Build the docker image
```bash
docker build -t offlineimap .
```

## Before the actual run

```bash
export EMAIL="my.username@gmail.com"
export NAME="My Name"
export CONNECTIONS=3

export OFFLINEIMAP="${HOME}/.offlineimap"
export LOGS="${OFFLINEIMAP}/${EMAIL}/logs/"
export MAILDIR="${OFFLINEIMAP}/${EMAIL}/Mail"

mkdir -vp ${LOGS}
mkdir -vp ${MAILDIR}
```

## Password

I have two step verification on for my google account. If you do, too, you
might want to set up an "App password" for offlineimap. To do this, log into
gmail, click your user in the top right corner. Click Manage your Google
Account. Click Security. Click App passwords. Then generate one for
offlineimap.

If you like to keep your dotfiles on github or some other publicly available
place, you might not like having your password just written out. Some people
get fancy and integrate with keyrings and such, but for this little project we
will template the password in `offlineimaprc` and replace it at startup.

To do so we will export the generated code (without spaces) to the `PASSWORD`
environment variable.

```bash
# remember to keep the space at the start of the line is important since it
# stops the command from being stored in your shell history.
 export PASSWORD="gmail_app_password"
```

## Get the emails
```bash
docker run -d --name offlineimap \
	-v ${LOGS}:/opt/log/offlineimap \
	-v ${MAILDIR}:/opt/var/mail \
	--env PASSWORD="${PASSWORD}" \
	--env EMAIL="${EMAIL}" \
	--env CONNECTIONS="${CONNECTIONS}" \
	--env NAME="${NAME}" \
	--restart=always \
	offlineimap
```
